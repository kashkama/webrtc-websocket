'use strict';

const express = require('express');
const socketServer = require('ws').Server;

const server = express();

const wss = new socketServer({ server });


function accept(req, res) {
	// all incoming request must be websockets
	if (!req.headers.upgrade || req.headers.upgrade.toLowerCase() != 'websocket') {
		res.end();
		return;
	}
	// can be connection: keep-alive, upgrade
	if (req.headers.connection.match(/\bupgrade\b/i)) {
		res.end();
		return;
	}

	wss.handleUpgrade(req, req.socket, Buffer.alloc(0), wss.on('connection', ws => {
		ws.send('{"connection": "ok"}')

		//when a message is received.
		ws.on('message', message => {
			//for each websocket client
			wss.clients.forEach(client => {
				client.send(`{"message" : ${message}}`)
			})
		})
	}));
}

exports.accept = accept;
